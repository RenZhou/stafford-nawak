<?php

function getList()
{
    $list = [];

    $ranges = [
        [
            'min' => 1,
            'max' => 15
        ],
        [
            'min' => 16,
            'max' => 30
        ], [
            'min' => 31,
            'max' => 45
        ], [
            'min' => 46,
            'max' => 60
        ], [
            'min' => 61,
            'max' => 75
        ],
    ];

    foreach ($ranges as $key => $range) {
        $list[$key] = [];
        for ($i = 0; $i < 5;) {
            $rvalue = rand($range['min'], $range['max']);
            if (!isset($list[$key]) or !in_array($rvalue, $list[$key])) {
                $list[$key][$i++] = $rvalue;
            }
        }
        sort($list[$key]);
    }
    $list[2][2] = 'Free';
    return $list;
}

$list = getList();

?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>PHP Assessment Exercise 3-1</title>
    <style>
        thead td {
            font-size: 20px;
        }

        td {
            width: 2em;
            border: 1px black solid
        }
    </style>
</head>

<body>
<section id="bingo">
    <h2>Bingo</h2>
    <table>
    <thead>
    <tr>
        <td>B</td>
        <td>I</td>
        <td>N</td>
        <td>G</td>
        <td>O</td>
    </tr>
    </thead>
    <tbody>
    <?php
    for ($j = 0; $j < 5; $j++) {
        echo "<tr>";
        for ($i = 0; $i < 5; $i++) {
            echo "<td>" . $list[$i][$j] . "</td>";
        }
        echo "</tr>";
    }
    ?>
    </tbody>
    </table>

    <form action="assessment3-1.php" method="post">
        <input type="submit" value="new">
    </form>
</section>

</body>
</html>