<?php

session_start();

function getList()
{
    $list = [];

    $ranges = [
        [
            'min' => 1,
            'max' => 15
        ],
        [
            'min' => 16,
            'max' => 30
        ], [
            'min' => 31,
            'max' => 45
        ], [
            'min' => 46,
            'max' => 60
        ], [
            'min' => 61,
            'max' => 75
        ],
    ];

    foreach ($ranges as $key => $range) {
        $list[$key] = [];
        for ($i = 0; $i < 5;) {
            $rvalue = rand($range['min'], $range['max']);
            if (!isset($list[$key]) or !in_array($rvalue, $list[$key])) {
                $list[$key][$i++] = $rvalue;
            }
            sort($list[$key]);
        }
    }
    $list[2][2] = 'Free';
    return $list;
}

function calcul($number1, $number2, $operator)
{
    switch ($operator) {
        case '+':
            return $number1 + $number2;
        case '-':
            return $number1 - $number2;
        case '*':
            return $number1 * $number2;
        case '/':
            return intval($number1 / $number2);
        case '=':
            return $number1;
    }
}

function login($username, $password)
{
    if ($username == 'Fred' and $password == 'Bloggs') {
        $_SESSION['username'] = $username;
    }
}

function logout()
{
    session_destroy();
    header("Location: assessment3-3.php");
}

if (isset($_POST["login"])) {
    login($_POST["username"], $_POST["password"]);
} elseif (isset($_POST["logout"])) {
    logout();
}

$list = getList();

if (isset($_POST['operation'])) {
    if (isset($_SESSION['value']) and !empty($_SESSION['value'])) {
        $_SESSION['value'] = calcul($_SESSION['value'], $_POST['value'], $_SESSION['operation']);
    } else {
        $_SESSION['value'] = $_POST['value'];
    }
    $_SESSION['operation'] = $_POST['operation'];
}

if (isset($_POST['reset'])) {
    session_destroy();
    header("Location: assessment3-3.php");
}

?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>PHP Assessment Exercise 3-3</title>
    <style>
        thead td {
            font-size: 20px;
        }

        td {
            width: 2em;
            border: 1px black solid
        }
    </style>
</head>

<body>
<section id="bingo">
    <h2>Bingo</h2>
    <table>
        <thead>
        <tr>
            <td>B</td>
            <td>I</td>
            <td>N</td>
            <td>G</td>
            <td>O</td>
        </tr>
        </thead>
        <tbody>
        <?php
        for ($j = 0; $j < 5; $j++) {
            echo "<tr>";
            for ($i = 0; $i < 5; $i++) {
                echo "<td>" . $list[$i][$j] . "</td>";
            }
            echo "</tr>";
        }
        ?>
        </tbody>
    </table>

    <form action="assessment3-3.php" method="post">
        <input type="submit" value="new">
    </form>
</section>
<br>

<section>
    <h2>Calculator</h2>
    <form action="assessment3-3.php" method="post">
        <label>
            <input type="number" name="value">
        </label>
        <input type="submit" value="+" name="operation">
        <input type="submit" value="-" name="operation">
        <input type="submit" value="*" name="operation">
        <input type="submit" value="/" name="operation">
        <input type="submit" value="=" name="operation">
        <input type="submit" value="reset" name="reset">
    </form>
    <p><?php if (isset($_SESSION['value'])) echo $_SESSION['value']; ?></p>
</section>

<section id="login">
    <h2>Login</h2>
    <form action="assessment3-3.php" method="post">
        <fieldset>
            <?php
            if (isset($_SESSION['username'])) {
                echo "<input type=\"submit\" name=\"logout\" value=\"logout\">";
            } else {
                echo
                "<label for=\"username\"> Login:
                    <input id='username' type=\"text\" name=\"username\">
                </label>
                <label for=\"password\"> Password:
                    <input id='password' type=\"password\" name=\"password\">
                </label>
                <input type=\"submit\" name=\"login\" value=\"login\">";
            }
            ?>
        </fieldset>
    </form>
    <?php
    echo '<a href="assessment3-3-2.php">Welcome Page</a>';
    if (isset($_SESSION['username'])) {
        echo "<a href=\"assessment3-3-3.php\">Secret Page</a>";
    }
    ?>
</section>

</body>
</html>