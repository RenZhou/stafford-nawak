<?php
session_start();

$info = [
    'name' => [
        'type' => 'text',
        'displayname' => 'name',
        'summary' => true
    ],
    'address' => [
        'type' => 'text',
        'displayname' => 'address',
        'summary' => true
    ],
    'birth' => [
        'type' => 'date',
        'displayname' => 'date of birthday',
        'summary' => true
    ],
    'username' => [
        'type' => 'text',
        'displayname' => 'username',
        'summary' => true
    ],
    'password' => [
        'type' => 'password',
        'displayname' => 'password',
        'summary' => false
    ],
    'mother' => [
        'type' => 'text',
        'displayname' => 'mother’s maiden name',
        'summary' => true
    ],
    'tc' => [
        'type' => 'checkbox',
        'displayname' => 'terms & conditions',
        'summary' => false
    ],
    'email' => [
        'type' => 'email',
        'displayname' => 'email address',
        'summary' => true
    ]
];

$_SESSION['form-part'] = 0;

?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>PHP Assessment Exercise 3-4</title>
</head>

<body>
<section id="form">
    <h2>Confirmation</h2>
    <ul>
        <?php
        foreach ($info as $name => $item) {
            echo "<input type='hidden' name='" . $name . "'";
            if (isset($_POST[$name])) {
                echo " value='" . $_POST[$name] . "'";
            }
            echo " >";
        }
        foreach ($_POST as $key => $value) {
            if (array_key_exists($key, $info)) {
                if ($info[$key]["summary"]) {
                    echo "<li>" . $info[$key]['displayname'] . " : " . $value . "</li>";
                }
            }
        }
        ?>
    </ul>
    <form action="<?php echo 'assessment3-4.php'; ?>" method="post">
        <?php
        foreach ($info as $name => $item) {
            echo "<input type='hidden' name='" . $name . "'";
            if (isset($_POST[$name])) {
                echo " value='" . $_POST[$name] . "'";
            }
            echo " >";
        }
        ?>
        <input type="submit" value="modify">
    </form>
</section>

</body>
</html>