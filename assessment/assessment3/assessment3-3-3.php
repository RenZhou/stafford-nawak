<?php
session_start();

?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <?php
    if (!isset($_SESSION['username'])) {
        echo '<meta http-equiv="refresh" content="5;URL=\'./assessment3-3.php\'" />';
    }
    ?>
    <title>PHP Assessment Exercise 3-3</title>
</head>

<body>

<section id="secret">

    <?php
    if (!isset($_SESSION['username'])) {
        echo "<h2>You have not permission to access this page</h2>";

        echo "<p>You will be redirect to main page in 5 seconds</p>";
    } else {
        echo "<h2>Actualy, There is nothing</h2>";
        echo "<a href='assessment3-3.php'> Main page </a>";
    }
    ?>

</section>

</body>
</html>