<?php
session_start();
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>PHP Assessment Exercise 3-3</title>
</head>

<body>
<section id="welcome">
    <h2>
        Welcome
        <?php
        if (isset($_SESSION['username'])) {
            echo $_SESSION['username'];
        } else {
            echo "... But I don't know you";
        }
        ?>
    </h2>
    <a href="assessment3-3.php">Main page</a>
</section>

</body>
</html>