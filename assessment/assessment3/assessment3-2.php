<?php

session_start();

function getList()
{
    $list = [];

    $ranges = [
        [
            'min' => 1,
            'max' => 15
        ],
        [
            'min' => 16,
            'max' => 30
        ], [
            'min' => 31,
            'max' => 45
        ], [
            'min' => 46,
            'max' => 60
        ], [
            'min' => 61,
            'max' => 75
        ],
    ];

    $biggerNumber = 5;
    foreach ($ranges as $key => $range) {
        $list[$key] = [];
        for ($i = 0; $i < $biggerNumber;) {
            $rvalue = rand($range['min'], $range['max']);
            if (!isset($list[$key]) or !in_array($rvalue, $list[$key])) {
                $list[$key][$i++] = $rvalue;
            }
            sort($list[$key]);
        }
    }
    $list[intval($biggerNumber / 2)][intval($biggerNumber / 2)] = 'Free';
    return $list;
}

function calcul($number1, $number2, $operator)
{
    switch ($operator) {
        case '+':
            return $number1 + $number2;
        case '-':
            return $number1 - $number2;
        case '*':
            return $number1 * $number2;
        case '/':
            if ($number2 == 0) {
                echo "You can't divise by 0";
                return $number1;
            }
            return intval($number1 / $number2);
        case '=':
            return $number1;
    }
}

$list = getList();

if (isset($_POST['operation'])) {
    if (isset($_SESSION['value']) and !empty($_SESSION['value'])) {
        $_SESSION['value'] = calcul($_SESSION['value'], $_POST['value'], $_SESSION['operation']);
    } else {
        $_SESSION['value'] = $_POST['value'];
    }
    $_SESSION['operation'] = $_POST['operation'];
}

if (isset($_POST['reset'])) {
    session_destroy();
    header("Location: assessment3-2.php");
}

?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>PHP Assessment Exercise 3-2</title>
    <style>
        thead td {
            font-size: 20px;
        }

        td {
            width: 2em;
            border: 1px black solid
        }
    </style>
</head>

<body>
<section id="bingo">
    <h2>Bingo</h2>
    <table>
        <thead>
        <tr>
            <td>B</td>
            <td>I</td>
            <td>N</td>
            <td>G</td>
            <td>O</td>
        </tr>
        </thead>
        <tbody>
        <?php
        for ($j = 0; $j < 5; $j++) {
            echo "<tr>";
            for ($i = 0; $i < 5; $i++) {
                echo "<td>" . $list[$i][$j] . "</td>";
            }
            echo "</tr>";
        }
        ?>
        </tbody>
    </table>

    <form action="assessment3-2.php" method="post">
        <input type="submit" value="new">
    </form>
</section>
<br>

<section>
    <h2>Calculator</h2>
    <form action="assessment3-2.php" method="post">
        <label>
            <input type="number" name="value">
        </label>
        <input type="submit" value="+" name="operation">
        <input type="submit" value="-" name="operation">
        <input type="submit" value="*" name="operation">
        <input type="submit" value="/" name="operation">
        <input type="submit" value="=" name="operation">
        <input type="submit" value="reset" name="reset">
    </form>
    <p><?php if (isset($_SESSION['value'])) echo $_SESSION['value']; ?></p>
</section>

</body>
</html>