<?php

session_start();
if (!isset($_SESSION['user'])) {
    include("view/welcome.php");
} else {
    include("controller/search.php");

    include("view/index.php");
}