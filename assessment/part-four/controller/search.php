<?php

include_once("model/vehicle.php");
include_once("model/favorite.php");

/**
 * This function initialize a array of filter
 *
 * @return mixed
 */
function get_filter_with_blank()
{
    $filter['make'] = isset($_GET['make']) ? strtolower($_GET['make']) : NULL;
    $filter['model'] = isset($_GET['model']) ? strtolower($_GET['model']) : NULL;
    $filter['year_equal'] = isset($_GET['year_equal']) ? $_GET['year_equal'] : NULL;
    $filter['year_greater'] = isset($_GET['year_greater']) ? $_GET['year_greater'] : NULL;
    $filter['year_less'] = isset($_GET['year_less']) ? $_GET['year_less'] : NULL;
    $filter['CC_equal'] = isset($_GET['CC_equal']) ? $_GET['CC_equal'] : NULL;
    $filter['CC_greater'] = isset($_GET['CC_greater']) ? $_GET['CC_greater'] : NULL;
    $filter['CC_less'] = isset($_GET['CC_less']) ? $_GET['CC_less'] : NULL;
    $filter['colour'] = isset($_GET['colour']) ? strtolower($_GET['colour']) : NULL;

    return $filter;
}

/**
 * This function save or remove a favorite in DB
 * @param $hasFilter
 * @param $filter
 * @return bool
 */
function saveFavorite($hasFilter, $filter)
{
    if (!$hasFilter) {
        $count = addFavorite($_SESSION['user'], $filter);
        if ($count == "00000") {
            $_SESSION['success'] = ["Save Favorite" => "Save favorite successful"];
            $hasFilter = !$hasFilter;
        } else {
            $_SESSION['exception'] = ["Save Favorite Failed" => "Favorite was not saved"];
            header('Location: ./index.php');
            die();
        }
    } else {
        $count = removeFavorite($_SESSION['user'], $filter);
        if ($count == "00000") {
            if (!isset($_SESSION["success"])) {
                $_SESSION["success"] = [];
            }
            $_SESSION['success'] = ["Remove Favorite" => "Remove favorite successful"];
            $hasFilter = !$hasFilter;
        } else {
            if (!isset($_SESSION["exception"])) {
                $_SESSION["exception"] = [];
            }
            $_SESSION['exception'] = ["yoyo check now" => "Favorite was not removed"];
        }
    }
    return $hasFilter;
}

if (isset($_GET["search_id"])) {
    // There is when we need to filter by search history
    $filter = getFilterByID($_GET['search_id']);
} elseif (!isset($_GET['favorite']) and !isset($_GET['search'])) {
    // There are default filter
    $filter = getDefaultFilter($_SESSION['user']);
    if (!$filter) {
        // If user doesn't have any favorite search
        $filter = get_filter_with_blank();
    }
} else {
    // There are first time showing
    $filter = get_filter_with_blank();
}

$hasFavorite = checkFavorite($_SESSION['user'], $filter);
$vehicles = get_vehicle($filter);
$colours = get_availble_colour();

if (isset($_GET['search']) or isset($_GET["search_id"])) {
    // if we search a car
    if ($hasFavorite) {
        // Increment favorite time
        updateFavoriteCount($hasFavorite['id']);
    }
} elseif (isset($_GET['favorite'])) {
    // if we need to save or remove a favorite search
    $hasFavorite = saveFavorite($hasFavorite, $filter);
}