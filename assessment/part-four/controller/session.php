<?php

include_once("model/user.php");

/**
 * get signup needed field
 * @return array
 */
function get_post_signup_field()
{
    $list = array(
        ":email" => $_POST['email'],
        ":password" => password_hash($_POST['password'], PASSWORD_DEFAULT),
        ":surname" => $_POST['surname'],
        ":forename" => $_POST['forename'],
        ":birthday" => $_POST['birthday'],
        ":address" => $_POST['address'],
        ":phonenumber" => $_POST['phonenumber']
    );
    return $list;
}

/**
 * get edit neeeded field
 * @return array
 */
function get_post_edit_field()
{
    $info = [];
    if (!empty($_POST['password'])) {
        $info['passoword'] = password_hash($_POST['password'], PASSWORD_DEFAULT);
    }
    if (!empty($_POST['surname'])) {
        $info['surname'] = $_POST['surname'];
    }
    if (!empty($_POST['forename'])) {
        $info['forename'] = $_POST['forename'];
    }
    if (!empty($_POST['birthday'])) {
        $info['birthday'] = $_POST['birthday'];
    }
    if (!empty($_POST['address'])) {
        $info['address'] = $_POST['address'];
    }
    if (!empty($_POST['phonenumber'])) {
        $info['phonenumber'] = $_POST['phonenumber'];
    }
    return $info;
}

/**
 * Login function
 */
function login()
{
    $user = user_login($_POST["email"], $_POST["password"]);
    if ($user) {
        $_SESSION['user'] = $user;
        echo "Connexion successful, redirect in 3 sec ...";
        header('Refresh: 3; URL = ./');
        die();
    } else {
        $_SESSION["exception"] = [403 => "Sign in failed"];
        header('Location: ./login.php');
        die();
    }
}

/**
 * @param $info
 */
function saveRegisterInfo($info)
{
    foreach ($info as $key => $value) {
        if ($key != "password") {
            setcookie($key, $value, time() + 3600);
        }

        if (!isset($_COOKIE[$key])) {
            echo "Cookie named '" . $key . "' is not set!<br>";
        } else {
            echo "Cookie '" . $key . "' is set!";
            echo "Value is: " . $_COOKIE[$key] . "<br>";
        }
    }
}

/**
 * register function
 */
function register()
{
    $info = get_post_signup_field();
    $register_code = user_register($info);

    if ($register_code == "00000") {
        echo "Update successful, redirect in 3 sec ...";
        header('Refresh: 3; URL = ./login.php');
        die();
    } else {
        switch ($register_code) {
            case "23000" :
                $_SESSION["exception"] = [23000 => "Email existed!"];
                break;
            default :
                $_SESSION['exception'] = [409 => "Something is wrong!"];
        }
        header('Location: ./signup.php');
        die();
    }
}

/**
 * edit information
 */
function edit()
{
    $info = get_post_edit_field();
    $update_nb = user_update($_SESSION['user'], $info);

    if ($update_nb > 0) {
        $user = get_user($_SESSION['user']['id']);
        $_SESSION['user'] = $user;
        echo "Update successful, redirect in 3 sec ...";
        header('Refresh: 3; URL = ./edit.php');
        die();
    } else {
        switch ($update_nb) {
            case "23000" :
                $_SESSION["exception"] = [23000 => "Email existed!"];
                break;
            default :
                $_SESSION["exception"] = [409 => "Something is wrong!"];
        }
        header('Location: ./edit.php');
        die();
    }
}

if (isset($_POST["login"])) {
    login();
} elseif (isset($_POST["signup"])) {
    register();
} elseif (isset($_POST['editRegistration'])) {
    edit();
}