<?php

include_once("model/favorite.php");

/**
 * @return array
 */
function getAllFavorites()
{
    return getAllFilter($_SESSION['user']);
}

if (isset($_POST['delete'])) {
    $count = removeFavoriteByID($_SESSION['user'], $_POST['favorite_id']);
} elseif (isset($_POST['removefavorite'])) {
    removeFavoriteFavorite($_SESSION['user'], $_POST['favorite_id']);
} elseif (isset($_POST['addfavorite'])) {
    $count = addFavoriteFavorite($_SESSION['user'], $_POST['favorite_id']);
}

$favorites = getAllFavorites();
