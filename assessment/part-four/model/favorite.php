<?php

include_once("connexion.php");

/**
 * @param $type_query
 * @param $filter
 * @return string
 */
function build_query_filter($type_query, $filter)
{
    global $dbh;
    $query = $type_query . " FROM `search_history` WHERE ";
    if (count($filter) == 0) {
        return $query . "1 = 1";
    }
    $clause = [];
    foreach ($filter as $key => $value) {
        if (!is_int($key)) {
            array_push($clause, $key . " = " . $dbh->quote($value));
        }
    }
    $condition = implode(" and ", $clause);
    $condition = str_replace("= ''", " is NULL", $condition);
    return $query . $condition;
}

/**
 * @param $filter
 * @return string
 */
function build_query_insert($filter)
{
    global $dbh;
    $query = "INSERT INTO search_history ";
    if (count($filter) == 0) {
        return $query . "1 = 1";
    }
    foreach ($filter as $key => $value) {
        $filter[$key] = $dbh->quote($value);
    }
    $keys = array_keys($filter);
    $champs = implode(", ", $keys);
    $values = implode(", ", $filter);

    $values = str_replace("''", 'NULL', $values);

    return $query . "(id, " . $champs . ", times) VALUES " . "(NULL, " . $values . ", 1)";
}

/**
 * @param $user
 * @param $filter
 * @return mixed
 */
function checkFavorite($user, $filter)
{
    global $dbh;

    $query = build_query_filter("SELECT * ", array_merge(["user_id" => $user['id']], $filter));

    return $dbh->query($query)->fetch();
}

/**
 * @param $user
 * @param $filter
 * @return mixed
 */
function addFavorite($user, $filter)
{
    global $dbh;

    $query = build_query_insert(array_merge(["user_id" => $user['id']], $filter));

    $dbh->exec($query);
    return $dbh->errorCode();
}

/**
 * @param $user
 * @param $filter
 * @return mixed
 */
function removeFavorite($user, $filter)
{
    global $dbh;

    $query = build_query_filter("DELETE ", array_merge(["user_id" => $user['id']], $filter));

    $dbh->exec($query);
    return $dbh->errorCode();
}

/**
 * @param $search_id
 * @return mixed
 */
function updateFavoriteCount($search_id)
{
    global $dbh;

    $query = "UPDATE search_history SET times = times + 1 WHERE id = " . $search_id;
    $dbh->exec($query);
    return $dbh->errorCode();
}

/**
 * @param $user
 * @return mixed
 */
function getDefaultFilter($user)
{
    global $dbh;

    $query = "SELECT * FROM search_history
              WHERE user_id = " . $user['id'] . " ORDER BY favorite desc, times desc limit 1";
    return $dbh->query($query)->fetch();
}

/**
 * @param $user
 * @return array
 */
function getAllFilter($user)
{
    global $dbh;

    $query = "SELECT * FROM search_history
              WHERE user_id = " . $user['id'] . " ORDER BY favorite desc, times desc";
    $result = $dbh->query($query);
    if ($result) {
        return $result->fetchAll();
    }
}

/**
 * @param $id
 * @return mixed
 */
function getFilterByID($id)
{
    global $dbh;

    $query = "SELECT * FROM search_history WHERE id = :id";
    $stmt = $dbh->prepare($query);
    $stmt->bindValue(':id', $id, PDO::PARAM_INT);
    $stmt->execute();
    return $stmt->fetch();
}

/**
 * @param $user
 * @param $search_id
 * @return bool
 */
function removeFavoriteByID($user, $search_id)
{
    global $dbh;

    $query = "DELETE FROM search_history WHERE id = :search_id AND user_id = :user_id";
    $stmt = $dbh->prepare($query);
    $stmt->bindValue(':search_id', $search_id, PDO::PARAM_INT);
    $stmt->bindValue(':user_id', $user['id'], PDO::PARAM_INT);
    return $stmt->execute();
}

/**
 * @param $user
 * @param $search_id
 */
function removeFavoriteFavorite($user, $search_id) {
    global $dbh;

    $stmt = $dbh->prepare("UPDATE search_history SET favorite = false WHERE id = :search_id and user_id = :user_id");
    $stmt->bindValue(':search_id', $search_id, PDO::PARAM_INT);
    $stmt->bindValue(':user_id', $user['id'], PDO::PARAM_INT);
    $stmt->execute();
}

/**
 * @param $user
 * @param $search_id
 * @return bool
 */
function addFavoriteFavorite($user, $search_id) {
    global $dbh;

    $query = "UPDATE search_history SET favorite = false WHERE favorite = true";
    $dbh->exec($query);

    $stmt = $dbh->prepare("UPDATE search_history SET favorite = true WHERE id = :search_id and user_id = :user_id");
    $stmt->bindValue(':search_id', $search_id, PDO::PARAM_INT);
    $stmt->bindValue(':user_id', $user['id'], PDO::PARAM_INT);
    return $stmt->execute();
}