<?php

include_once("connexion.php");

/**
 * @param $username
 * @param $password
 * @return bool|mixed
 */
function user_login($username, $password)
{
    global $dbh;

    $username_quote = $dbh->quote($username);
    $query = "SELECT * FROM user WHERE email = " . $username_quote;
    $stmt = $dbh->query($query);
    $result = $stmt->fetch();

    if (!password_verify($password, $result['password'])) {
        return false;
    }

    $login_stmt = $dbh->prepare("SELECT `id`, `surname`, `forename`, `birthday`, `address`, `email`, `phonenumber` FROM `user` WHERE `email` = :email");

    $login_stmt->execute(array(':email' => $username));
    return $login_stmt->fetch();
}

/**
 * @param $info
 * @return string
 */
function user_register($info)
{
    global $dbh;
    $stmt = $dbh->prepare("INSERT INTO `user`(`id`, `surname`, `forename`, `birthday`, `address`, `email`, `phonenumber`, `password`) VALUES (NULL,:surname,:forename,:birthday,:address,:email,:phonenumber,:password)");

    $stmt->execute($info);

    return $stmt->errorCode();
}


/**
 * @param $user
 * @param $info
 * @return int
 */
function user_update($user, $info)
{
    global $dbh;
    $query = "UPDATE user SET ";
    $var = [];
    foreach ($info as $key => $value) {
        array_push($var, $key . " = " . $dbh->quote($value));
    }
    $query .= implode(', ', $var);
    $query .= " WHERE id = " . $dbh->quote($user['id']);

    return $dbh->exec($query);
}

/**
 * @param $user_id
 * @return mixed
 */
function get_user($user_id)
{
    global $dbh;
    $query = "SELECT `id`, `surname`, `forename`, `birthday`, `address`, `email`, `phonenumber` FROM `user` WHERE id = " . $user_id;

    return $dbh->query($query)->fetch();
}