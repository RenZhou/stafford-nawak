<?php

include_once('connexion.php');

/**
 * @param $filter
 * @return string
 */
function build_query($filter)
{
    global $dbh;
    $query = "SELECT * FROM `vehicle` WHERE ";
    $clause = [];
    if (!empty($filter['make'])) {
        array_push($clause, "make LIKE " . $dbh->quote("%" . $filter["make"] . "%"));
    }
    if (!empty($filter['model'])) {
        array_push($clause, "model LIKE " . $dbh->quote("%" . $filter["model"] . "%"));;
    }
    $clause = get_clause($clause, $filter, "year");
    $clause = get_clause($clause, $filter, "CC");
    if (!empty($filter['colour'])) {
        array_push($clause, "colour LIKE " . $dbh->quote("%" . $filter["colour"] . "%"));
    }
    if (count($clause) == 0) {
        return $query . "1 = 1 ORDER BY year asc, cc desc, colour asc";
    }
    return $query . implode(" and ", $clause) . " ORDER BY year asc, cc desc, colour asc";
}

/**
 * @param $clause
 * @param $filter
 * @param $item
 * @return mixed
 */
function get_clause($clause, $filter, $item)
{
    $hasEqual = !empty(($filter[$item . "_equal"]));
    $hasGreater = !empty(($filter[$item . "_greater"]));
    $hasLess = !empty(($filter[$item . "_less"]));

    if ($hasEqual or $hasGreater or $hasLess) {
        $sql = "";
        if ($hasEqual) {
            $sql = "(" . $item . " = " . $filter[$item . "_equal"] . ")";
        }
        if ($hasGreater or $hasLess) {
            if ($hasEqual) {
                $sql .= " or ";
            }
            $sql .= "(";
            if ($hasGreater) {
                $sql .= $item . " > " . $filter[$item . "_greater"];
                if ($hasLess) {
                    $sql .= " and ";
                } else {
                    $sql .= ")";
                }
            }
            if ($hasLess) {
                $sql .= $item . " < " . $filter[$item . "_less"] . ")";
            }
        }
        if ($hasEqual and ($hasGreater or $hasLess)) {
            $sql = "(" . $sql . ")";
        }
        array_push($clause, $sql);
    }
    return $clause;
}

/**
 * @param $filter
 * @return array|PDOStatement
 */
function get_vehicle($filter)
{
    global $dbh;
    $query = build_query($filter);

    $result = $dbh->query($query, PDO::FETCH_ASSOC);
    if($result){
        return $result;
    }
    return [];
}

/**
 * @return PDOStatement
 */
function get_availble_colour()
{
    global $dbh;

    return $dbh->query("SELECT colour FROM vehicle GROUP BY colour");

}