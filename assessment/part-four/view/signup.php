<!DOCTYPE html>
<html>
<head>
    <title>Number One Antique Car Trade</title>
    <link rel="stylesheet" href="public/css/common.css">
</head>

<body>

<?php include("header.php"); ?>

<section class="first_section">
    <header>
        <h2>Registration</h2>
    </header>
    <form action="signup.php" method="post">
        <fieldset>
            <legend>Required field</legend>
            <label> <span>Email (Will be your username):</span>
                <input type="email" required="required" name="email" <?php if(isset($_POST['email'])) echo "value=" . $_POST['email'];?>>
            </label>
            <label> <span>Password :</span>
                <input type="password" required="required" name="password" <?php if(isset($_POST['password'])) echo "value=" . $_POST['password'];?>>
            </label>
            <label> <span>Surname :</span>
                <input type="text" pattern="[A-Za-z ]*" name="surname"
                       required="required" <?php if (isset($_POST['surname'])) echo "value=" . $_POST['surname']; ?>>
            </label>
            <label> <span>Forename :</span>
                <input type="text" pattern="[A-Za-z ]*" name="forename"
                       required="required" <?php if (isset($_POST['forename'])) echo "value=" . $_POST['forename']; ?>>
            </label>
            <label> <span>Birthday :</span>
                <input type="date" name="birthday" required="required"
                       value="1970-01-01" <?php if (isset($_POST['birthday'])) echo "value=" . $_POST['birthday']; ?>>
            </label>
            <label> <span>Address :</span>
                <input type="text" name="address"
                       required="required" <?php if (isset($_POST['address'])) echo "value=" . $_POST['address']; ?>>
            </label>
            <label> <span>Phone number :</span>
                <input type="text" pattern="[0-9]{11}" maxlength="11" name="phonenumber"
                       required="required" <?php if (isset($_POST['phonenumber'])) echo "value=" . $_POST['phonenumber']; ?>>
            </label>
        </fieldset>
        <input type="submit" name="signup" value="Sign Up">
        <a href="login.php">I have already an account !</a>
    </form>
</section>

<?php include("footer.php"); ?>

</body>

</html>