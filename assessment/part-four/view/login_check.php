<?php

$time = $_SERVER['REQUEST_TIME'];
$timeout_duration = 1800;

if (isset($_SESSION['LAST_ACTIVITY']) && ($time - $_SESSION['LAST_ACTIVITY']) > $timeout_duration) {
    session_unset();
    session_destroy();
    session_start();
}

$_SESSION['LAST_ACTIVITY'] = $time;

if (!isset($_SESSION['user'])) {
    echo "You need to sign in to view this page, and you will be automaticly redirect in 5 sec ...";
    header('Refresh: 5; URL = login.php');
    die();
}