<section id="section_vehicle">
    <?php
        echo "<header>";
    if (count($vehicles) == 0) {
        echo "<h4>Sorry, there are no result</h4>";
    }else {
        echo "<h4>I find this for you</h4>";
    }
        echo "</header>";
    foreach ($vehicles as $vehicle) {
        ?>
        <article>
            <header>
                <h3><?php echo $vehicle['make'] ?> - <?php echo $vehicle['model'] ?></h3>
            </header>
            <div class="content">
                <img src="<?php echo $vehicle['picture_link'] ?>"
                     alt="<?php echo $vehicle['make'] ?> - <?php echo $vehicle['model'] ?>">
                <ul>
                    <li><span>Year : </span> <?php echo $vehicle['year'] ?> </li>
                    <li><span>Capacity (cc) : </span> <?php echo $vehicle['CC'] ?></li>
                    <li><span>Colour : </span> <?php echo $vehicle['colour'] ?></li>
                </ul>
            </div>
        </article>
        <?php
    }
    ?>
</section>
