<section id="favorite">
    <header>
        <h2>All My Saved Search Manager</h2>
    </header>
    <?php
    if (count($favorites) > 0) { ?>
        <table>
            <thead>
            <tr>
                <td>Make</td>
                <td>Model</td>
                <td>Year equal</td>
                <td>Year greater</td>
                <td>Year less</td>
                <td>CC equal</td>
                <td>CC greater</td>
                <td>CC less</td>
                <td>Colour</td>
                <td>Times</td>
                <td>Action</td>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($favorites as $favorite) {
                ?>
                <tr>
                    <td><?php echo isset($favorite['make']) ? $favorite['make'] : "-"; ?></td>
                    <td><?php echo isset($favorite['model']) ? $favorite['model'] : "-"; ?></td>
                    <td><?php echo isset($favorite['year_equal']) ? $favorite['year_equal'] : "-"; ?></td>
                    <td><?php echo isset($favorite['year_greater']) ? $favorite['year_greater'] : "-"; ?></td>
                    <td><?php echo isset($favorite['year_less']) ? $favorite['year_less'] : "-"; ?></td>
                    <td><?php echo isset($favorite['CC_equal']) ? $favorite['CC_equal'] : "-"; ?></td>
                    <td><?php echo isset($favorite['CC_greater']) ? $favorite['CC_greater'] : "-"; ?></td>
                    <td><?php echo isset($favorite['CC_less']) ? $favorite['CC_less'] : "-"; ?></td>
                    <td><?php echo isset($favorite['colour']) ? $favorite['colour'] : "-"; ?></td>
                    <td><?php echo isset($favorite['times']) ? $favorite['times'] : "-"; ?></td>
                    <td>
                        <!--                            <button formaction="edit.php?setDefault=-->
                        <?php //echo $favorite['id']; ?><!--">+</button>-->
                        <form action="edit.php" method="post">
                            <button class="action" name="delete">
                                <img src="public/img/remove.png" alt="remove">
                            </button>
                            <button class="action" formaction="index.php?search_id=<?php echo $favorite['id']; ?>">
                                <img src="public/img/search.png" alt="search">
                            </button>
                            <?php if ($favorite['favorite']) { ?>
                                <button class="action" name="removefavorite">
                                    <img src="public/img/heart.png" alt="remove favorite">
                                </button>
                            <?php } else { ?>
                                <button class="action" name="addfavorite">
                                    <img src="public/img/unheart.png" alt="add favorite">
                                </button>
                            <?php } ?>
                            <input type="hidden" name="favorite_id" value="<?php echo $favorite['id']; ?>">
                        </form>
                    </td>
                </tr>
                <?php
            } ?>
            </tbody>
        </table>
    <?php } else {
        echo "<h3>You have any favorite search saved ╮(╯▽╰)╭</h3>";
    } ?>
    <form action="index.php">
        <button id="search_more">Search More !</button>
    </form>
</section>