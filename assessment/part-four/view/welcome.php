<!DOCTYPE html>
<html>
<head>
    <title>Number One Antique Car Trade</title>
    <link rel="stylesheet" href="public/css/common.css">
    <link rel="stylesheet" href="public/css/footer.css">
</head>

<body>

<?php include("header.php"); ?>

<section id='welcome'>
    <header>
        <h2>Welcome</h2>
    </header>
    <main>
        <h4>This is a site for you. You can find your dream antique car here</h4>
        <ul>
            <li>
                <a href="login.php"> ↗ I want continue my search !</a>
            </li>
            <li>
                <a href="signup.php"> ↗ No account? Need a account?</a>
            </li>
        </ul>
    </main>
</section>

<?php include("footer.php"); ?>

</body>

</html>