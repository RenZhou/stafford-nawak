<!DOCTYPE html>
<html>
<head>
    <title>Number One Antique Car Trade</title>
    <link rel="stylesheet" href="public/css/common.css">
</head>

<body>

<?php include("header.php"); ?>

<section id="login">
    <header>
        <h2>Login Page</h2>
    </header>
    <form action="login.php" method="post">
        <label for="email"><p>Email :</p></label>
        <input type="email" required="required" name="email" placeholder="Email">

        <label for="password"><p>Password :</p></label>
        <input type="password" required="required" name="password" placeholder="Password">

        <input type="submit" name="login" value="Sign In">
        <a href="signup.php">No account? Need an account?</a>
    </form>
</section>

<?php include("footer.php"); ?>

</body>

</html>