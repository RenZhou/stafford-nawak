<section id="edit" class="first_section">
    <header>
        <h2>Edit Registration</h2>
    </header>
    <form action="edit.php" method="post" autocomplete="off">
        <fieldset>
            <legend>Required field</legend>
            <label> <span>Email :</span>
                <input type="email" disabled="disabled" name="email"
                       value="<?php echo $_SESSION['user']['email']; ?>">
            </label>
            <label> <span>Password :</span>
                <input type="password" name="password" placeholder="Change password">
            </label>
            <label> <span>Surname :</span>
                <input type="text" pattern="[A-Za-z ]*" name="surname" required="required"
                       value="<?php echo $_SESSION['user']['surname']; ?>">
            </label>
            <label> <span>Forename :</span>
                <input type="text" pattern="[A-Za-z ]*" name="forename" required="required"
                       value="<?php echo $_SESSION['user']['forename']; ?>">
            </label>
            <label> <span>Birthday :</span>
                <input type="date" name="birthday" required="required"
                       value="<?php echo $_SESSION['user']['birthday']; ?>">
            </label>
            <label> <span>Address :</span>
                <input type="text" name="address" required="required"
                       value="<?php echo $_SESSION['user']['address']; ?>">
            </label>
            <label> <span>Phone number :</span>
                <input type="text" pattern="0[0-9]{10}" name="phonenumber" required="required"
                       value="<?php echo $_SESSION['user']['phonenumber']; ?>">
            </label>
        </fieldset>
        <input type="submit" name="editRegistration" value="Modify">
    </form>
</section>