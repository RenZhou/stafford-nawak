<header>
    <div id="header-logo">
        <h3><a href="index.php">The Best Antique Car Trader</a></h3>
    </div>
    <?php
    if (!isset($_SESSION['user'])) {
        ?>
        <div id="header-link">
            <ul>
                <li><a href="login.php">Sign In</a></li>
                <li><a href="signup.php">Sign Up</a></li>
            </ul>
        </div>
    <?php } else { ?>
        <div id="header-link" class="connected">
            <div id="header-content">
                <p> Hello <?php echo $_SESSION['user']['surname']; ?> !</p>
                <ul>
                    <li><a href="edit.php">Edit</a></li>
                    <li><a href="logout.php">Log out</a></li>
                </ul>
            </div>
        </div>
    <?php } ?>
</header>
<?php

if (isset($_SESSION['exception'])) {
    echo "<div class='message error'><ul>";
    foreach ($_SESSION['exception'] as $key => $msg) {
        ?>
        <li><?php echo $msg; ?></li>
        <?php
    }
    echo "</div></ul>";
    unset($_SESSION['exception']);
}
?>

<?php if (isset($_SESSION['success'])) {
    echo "<div class='message success'><ul>";
    foreach ($_SESSION['success'] as $key => $msg) {
        ?>
        <li><?php echo $msg; ?></li>
        <?php
    }
    echo "</div></ul>";
    unset($_SESSION['success']);
}
?>
