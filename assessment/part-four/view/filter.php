<?php
$maxYear = date("Y");
?>

<section id="section_filter" class="first_section">
    <header>
        <h3>Choise your favorite car</h3>
    </header>
    <form id="form_favorite" action="index.php" method="get" autocomplete="on">
        <fieldset>
            <label for="make">
                <span>Make :</span> <input id="make" type="text" name="make"
                                           value="<?php if (isset($filter['make'])) echo $filter['make']; ?>">
            </label>
            <label for="model">
                <span>Model :</span><input id="model" type="text" name="model"
                                           value="<?php if (isset($filter['model'])) echo $filter['model']; ?>">
            </label>
            <label for="form_colour">
                <span>Prefere colour :</span>
                <select name="colour" id="form_colour">
                    <option value="">Anyone</option>
                    <?php
                    foreach ($colours as $colour) {
                        ?>
                        <option <?php if(isset($filter['colour']) and strtolower($filter['colour']) == strtolower($colour['colour'])) echo 'selected="selected"' ?>>
                            <?php echo ucfirst($colour['colour']); ?>
                        </option>
                    <?php } ?>
                </select>
            </label>
        </fieldset>
        <fieldset>
            <legend>Year</legend>
            <label for="year_equal" class="year">
                <span>Equal to :</span>
                <input id="year_equal" type="number" name="year_equal" min="1970" max="<?php echo $maxYear ?>"
                       value="<?php if (isset($filter['year_equal'])) echo $filter['year_equal']; ?>">
            </label>
            <label for="year_greater" class="year">
                <span>Greater than :</span>
                <input id="year_greater" type="number" name="year_greater" min="1970" max="<?php echo $maxYear ?>"
                       value="<?php if (isset($filter['year_greater'])) echo $filter['year_greater']; ?>">
            </label>
            <label for="year_less" class="year">
                <span>Less than :</span>
                <input id="year_less" type="number" name="year_less" min="1970" max="<?php echo $maxYear ?>"
                       value="<?php if (isset($filter['year_less'])) echo $filter['year_less']; ?>">
            </label>
        </fieldset>
        <fieldset>
            <legend> Cylinder Bore Capacity (cc)</legend>
            <label for="CC_equal" class="cc">
                <span>Equal to :</span>
                <input id="CC_equal" type="number" name="CC_equal" min="0"
                       value="<?php if (isset($filter['CC_equal'])) echo $filter['CC_equal']; ?>">
            </label>
            <label for="CC_greater" class="cc">
                <span>Greater than :</span>
                <input id="CC_greater" type="number" name="CC_greater" min="0"
                       value="<?php if (isset($filter['CC_greater'])) echo $filter['CC_greater']; ?>">
            </label>
            <label for="CC_less" class="cc">
                <span>Less than :</span>
                <input id="CC_less" type="number" name="CC_less" min="0"
                       value="<?php if (isset($filter['CC_less'])) echo $filter['CC_less']; ?>">
            </label>
        </fieldset>
        <div class="input_group">
            <input type="hidden" name="favorite">
            <input type="submit" name="search" value="Search">
            <?php if ($hasFavorite) {
                ?>
                <button id="btn_favorite" type="submit"><img src="public/img/favorite_saved.ico"
                                                             alt="Click to remove favorite"></button>
            <?php } else { ?>
                <button id="btn_favorite" type="submit"><img src="public/img/favorite.ico" alt="Click to save favorite">
                </button>
            <?php } ?>
        </div>
    </form>
</section>