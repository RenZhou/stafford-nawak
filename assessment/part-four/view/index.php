<?php
include_once("login_check.php")
?>

<!DOCTYPE html>
<html>
<head>
    <title>Number One Antique Car Trade</title>
    <link rel="stylesheet" href="public/css/common.css">
</head>

<body>

<?php include("header.php"); ?>

<?php include("filter.php"); ?>

<?php include("content.php") ?>

<?php include("footer.php"); ?>

</body>

</html>