<?php

require("./controller/search.php");

class SearchTest extends PHPUnit_Framework_TestCase
{
    /**
     * @group search
     */
    public function test_get_filter_with_blank_without_post(){
        $filter = get_filter_with_blank();
        $result['make']         = NULL;
        $result['model']        = NULL;
        $result['year_equal']   = NULL;
        $result['year_greater'] = NULL;
        $result['year_less']    = NULL;
        $result['CC_equal']     = NULL;
        $result['CC_greater']   = NULL;
        $result['CC_less']      = NULL;
        $result['colour']       = NULL;

        $this->assertEquals($result, $filter);
    }
}
