-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 25, 2016 at 01:22 PM
-- Server version: 5.5.8
-- PHP Version: 5.6.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `z034339f`
--

-- --------------------------------------------------------

--
-- Table structure for table `search_history`
--

CREATE TABLE IF NOT EXISTS `search_history` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `make` varchar(20) DEFAULT NULL,
  `model` varchar(20) DEFAULT NULL,
  `year_equal` year(4) DEFAULT NULL,
  `year_greater` year(4) DEFAULT NULL,
  `year_less` year(4) DEFAULT NULL,
  `CC_equal` int(11) DEFAULT NULL,
  `CC_greater` int(11) DEFAULT NULL,
  `CC_less` int(11) DEFAULT NULL,
  `colour` text,
  `times` int(11) NOT NULL,
  `favorite` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=136 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `search_history`
--

INSERT INTO `search_history` (`id`, `user_id`, `make`, `model`, `year_equal`, `year_greater`, `year_less`, `CC_equal`, `CC_greater`, `CC_less`, `colour`, `times`, `favorite`) VALUES
(134, 75, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0),
(135, 75, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'black', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` bigint(20) NOT NULL,
  `surname` varchar(20) DEFAULT NULL,
  `forename` varchar(20) DEFAULT NULL,
  `birthday` date DEFAULT '1970-01-01',
  `address` varchar(60) DEFAULT NULL,
  `email` varchar(60) NOT NULL,
  `phonenumber` varchar(11) NOT NULL,
  `password` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `surname`, `forename`, `birthday`, `address`, `email`, `phonenumber`, `password`) VALUES
(75, 'zhou', 'ren', '1994-01-04', '66 street queensville', 'zhou_ren.fr@hotmail.com', '07123456789', '$2y$10$cF30fxiSVYR7Pi5dGG6La.5Uy/8fPLw2dDLTzmmCE85iw2T54Z2Ty'),
(77, 'fas', 'fsd', '1970-01-01', 'fasd', 'zhou_ren.fr@hotmail.comff', '45646513213', '$2y$10$cF30fxiSVYR7Pi5dGG6La.5Uy/8fPLw2dDLTzmmCE85iw2T54Z2Ty');

-- --------------------------------------------------------

--
-- Table structure for table `vehicle`
--

CREATE TABLE IF NOT EXISTS `vehicle` (
  `id` bigint(20) NOT NULL,
  `make` varchar(20) NOT NULL,
  `model` varchar(20) NOT NULL,
  `year` year(4) DEFAULT NULL,
  `CC` int(11) DEFAULT NULL,
  `colour` varchar(15) DEFAULT NULL,
  `picture_link` text
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicle`
--

INSERT INTO `vehicle` (`id`, `make`, `model`, `year`, `CC`, `colour`, `picture_link`) VALUES
(4, 'Mercedes', '380 SL', 1983, 3800, 'black', 'http://assets.lespac.com/binary/detBig/162922061.jpg'),
(5, 'Pontiac', 'GTO', 1967, 3500, 'black', 'http://assets.lespac.com/binary/detBig/159742616.jpg'),
(6, 'Suzuki', 'Convertible', 1992, 12300, 'red', 'http://assets.lespac.com/binary/detBig/168255569.jpg'),
(7, 'Mercury', 'Comet', 1976, 4000, 'blue', 'http://assets.lespac.com/binary/detBig/172948252.jpg'),
(8, 'Chevrolet', 'Corvette', 1992, 5700, 'white', 'http://assets.lespac.com/binary/detBig/162817273.jpg'),
(9, 'Ford', 'Mustang', 2007, 4600, 'yellow', 'http://assets.lespac.com/binary/detBig/162746221.jpg'),
(10, 'Ford', 'Roadster', 1931, 5700, 'red', 'http://assets.lespac.com/binary/detBig/168780013.jpg'),
(11, 'Chevrolet', 'Blazer', 1989, 5700, 'Orange', 'http://assets.lespac.com/binary/detBig/168425697.jpg'),
(12, 'Pontiac', 'Transam', 1977, 7000, 'Black', 'http://assets.lespac.com/binary/detBig/168745083.jpg'),
(13, 'Chrysler', 'Prowler', 2000, 5000, 'red', 'http://assets.lespac.com/binary/detBig/162117672.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `search_history`
--
ALTER TABLE `search_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_email` (`email`);

--
-- Indexes for table `vehicle`
--
ALTER TABLE `vehicle`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `search_history`
--
ALTER TABLE `search_history`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=136;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=78;
--
-- AUTO_INCREMENT for table `vehicle`
--
ALTER TABLE `vehicle`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `search_history`
--
ALTER TABLE `search_history`
  ADD CONSTRAINT `FK_USER_SEARCH` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
