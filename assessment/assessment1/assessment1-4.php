<?php
$title = "Mr";
$name = "Philip Windridge";
$email = "p.c.windridge@staffs.ac.uk";
$address[0] = "The Ocatagon";
$address[1] = "Beaconside";
$address[2] = "Stafford";
$postcode = "ST18 0DG";
$telephone = "01785 353419";

$all_value["title"] = $title;
$all_value["name"] = $name;
$all_value["email"] = $email;
$all_value["address0"] = $address[0];
$all_value["address1"] = $address[1];
$all_value["address2"] = $address[2];
$all_value["postcode"] = $postcode;
$all_value["telephone"] = $telephone;

foreach ($all_value as $key => $value) {
    if (isset($_POST[$key])) {
        $all_value[$key] = $_POST[$key];
    }
}

$personCharge = array('Nobody',
    'Russell Campion',
    'Fiona Knight',
    'Philip Windridge',
    'Fiona Knight',
    'Russell Campion',
    'Nobody');

$moduleNames = array('Web Design and Development',
    'Introduction to Web Programming',
    'Introduction to Web Development',
    'Introduction to Software Development',
    'Object Oriented and Event Driven Programming',
    'Hardware, Networks and Servers',
    'Maths For Interactive Computing',
    'System Modelling',
    'Introduction to Design Concepts for Computing',
    'Introduction to Digital Media');

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>PHP Assessment Exercise 1-4</title>
    <link rel="stylesheet" href="common.css">
</head>

<body>
<h1>PHP Assessment Exercises: Set 1</h1>


<section id="contact">
    <h2>Contact Details</h2>
    <address>
        <?php
        echo implode(" ", $all_value);
        ?>
    </address>
</section>

<section id="amendment">
    <h2>Details Amendment</h2>
    <form action="assessment1-4.php" method="post">
        <fieldset>
            <legend>Update Details</legend>
            <?php
            foreach ($all_value as $key => $value) {
                if (is_array($value)) {
                    foreach ($value as $index => $item) {
                        echo '<label><input type = "text" value = "' . $item . '" name = "' . $index . '" /></label>';
                    }
                } else {
                    echo '<label><input type = "text" value = "' . $value . '" name = "' . $key . '" /></label>';
                }
            }
            ?>
            <br>
            <input type="submit" value="Update">
        </fieldset>
    </form>
</section>

<section id="message" class="clearfix">
    <h2>Message of the Day</h2>
    <p>Today is <?php echo date("l"); ?> and the person in charge is <?php echo $personCharge[date("w")]; ?>!</p>
</section>

<section id="module">
    <h2>The following modules are available at Level 4</h2>
    <ul>
        <?php
        sort($moduleNames);
        foreach ($moduleNames as $name) {
            echo "<li>" . $name . "</li>";
        }
        ?>
    </ul>
</section>

</body>

</html>