<!DOCTYPE html>
<?php

$title = "Mr";
$name = "Philip Windridge";
$email = "p.c.windridge@staffs.ac.uk";
$address[0] = "The Ocatagon";
$address[1] = "Beaconside";
$address[2] = "Stafford";
$postcode = "ST18 0DG";
$telephone = "01785 353419";
?>

<html>

<head>
    <meta charset="UTF-8">
    <title>PHP Assessment Exercise 1-1</title>
</head>

<body>
<h1>PHP Assessment Exercises: Set 1</h1>

<section id="contact">
<h2>Contact Details</h2>
    <address>
        <?php
        echo $title . " " .
            $name . " " .
            $email . " " .
            $address[0] . " " .
            $address[1] . " " .
            $address[2] . " " .
            $postcode . " " .
            $telephone;
        ?>
    </address>
</section>
</body>
</html>