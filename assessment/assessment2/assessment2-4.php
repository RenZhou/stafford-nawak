<?php

session_start();
if (!isset($_SESSION["col"])) {
    $_SESSION["col"] = 10;
    $_SESSION["row"] = 10;
}

if (isset($_GET['col']) and isset($_GET['row'])) {
    $_SESSION['col'] = $_GET['col'];
    $_SESSION['row'] = $_GET['row'];
}

$all_level = [
    "undergraduate" => [
        "first" => [
            "grade-min" => 70,
            "grade-max" => 100,
            "message" => "70%+ Grade Points 13 - 15 First Class Honours",
        ],
        "second" => [
            "grade-min" => 60,
            "grade-max" => 69,
            "message" => "60 - 69% Grade Points 10 - 12 Upper Second Class Honours",
        ],
        "third" => [
            "grade-min" => 50,
            "grade-max" => 59,
            "message" => "50 - 59% Grade Points 7 - 9 Lower Second Class Honours",
        ],
        "fourth" => [
            "grade-min" => 40,
            "grade-max" => 49,
            "message" => "40 - 49% Grade Points 4 - 6 Third Class Honours",
        ],
        "fifth" => [
            "grade-min" => 0,
            "grade-max" => 39,
            "message" => " Fail",
        ]
    ],
    "hnd" => [
        "first" => [
            "grade-min" => 70,
            "grade-max" => 100,
            "message" => "70%+ Distinction - grade points 13 - 15 ",
        ],
        "second" => [
            "grade-min" => 53,
            "grade-max" => 69,
            "message" => "53 - 69% Merit - grade points 8 - 12 ",
        ],
        "third" => [
            "grade-min" => 40,
            "grade-max" => 52,
            "message" => "40 - 52% Pass - grade points 4 - 7",
        ],
        "fourth" => [
            "grade-min" => 0,
            "grade-max" => 39,
            "message" => "0 - 39% Fail - grade points 1 - 3",
        ]
    ],
    "masters" => [
        "first" => [
            "grade-min" => 70,
            "grade-max" => 100,
            "message" => "70%+ Distinction - grade points 13 - 15",
        ],
        "second" => [
            "grade-min" => 60,
            "grade-max" => 69,
            "message" => "60 - 69% Pass with Merit - grade points 10 - 12",
        ],
        "third" => [
            "grade-min" => 50,
            "grade-max" => 59,
            "message" => "50 - 59% Pass - grade points 7 - 9",
        ],
        "fourth" => [
            "grade-min" => 0,
            "grade-max" => 49,
            "message" => "0 - 49% Failure - grade points 1 – 6",
        ]
    ]
];

function getMessage($grade, $level)
{
    foreach ($level as $base) {
        if ($grade >= $base["grade-min"] and $grade <= $base["grade-max"]) {
            return $base["message"];
        }
    }
}


const NBMAX = 100;
const NBMIN = 1;
const NBTRY = 5;

function initGame()
{
    session_reset();
    $_SESSION["random"] = rand(NBMIN, NBMAX);
    $_SESSION["max"] = NBMAX;
    $_SESSION["min"] = NBMIN;
    $_SESSION["nbtry"] = 0;
}

if (!isset($_SESSION["random"])) {
    initGame();
}

if (isset($_POST["reset"])) {
    initGame();
}

$msg = "";
if ($_SESSION["nbtry"] < NBTRY) {
    if (isset($_POST["try"])) {
        $msg = "<p>You have tried " . ++$_SESSION["nbtry"] . " time (s)";
        if ($_POST["guess_value"] == $_SESSION["random"]) {
            $msg = "<p>Win</p>";
        } else {
            if ($_POST["guess_value"] > $_SESSION["random"]) {
                $_SESSION["max"] = $_SESSION["max"] >= $_POST["guess_value"] ? $_POST["guess_value"] - 1 : $_SESSION["max"];
                $msg .= " Number is smaller";
            } else {
                $_SESSION["min"] = $_SESSION["min"] <= $_POST["guess_value"] ? $_POST["guess_value"] + 1 : $_SESSION["min"];
                $msg .= " Number is bigger";
            }
            if ($_SESSION["nbtry"] >= NBTRY) {
                $msg = "<p>Game Over, number was " . $_SESSION["random"] . " please reset the game</p>";
            }
        }
    }
} else {
    $msg = "<p>Game Over, number was " . $_SESSION["random"] . ", please reset the game</p>";
}

$_SESSION["nbInput"] = 3;

if (isset($_POST["nbform"])) {
    $_SESSION["nbInput"] = $_POST["nbinput"];
}

?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>PHP Assessment Exercise 2-4</title>
    <link rel="stylesheet" href="common.css">
</head>

<body>
<section id="multiplication">
    <h2>Table of multiplication</h2>
    <form action="assessment2-1.php" method="get">
        <label for="col">
            <input id="col" type="number" name="col">
        </label>
        <label for="row">
            <input id="row" type="number" name="row">
        </label>
        <input type="submit" value="submit">
    </form>
    <br>
    <table>
        <tbody>
        <?php
        for ($col = 1; $col <= $_SESSION["col"]; ++$col) {
            echo "<tr>";
            for ($row = 1; $row <= $_SESSION["row"]; ++$row) {
                echo "<td>" . $col * $row . "</td>";
            }
            echo "</tr>";
        }
        ?>
        </tbody>
    </table>
</section>

<section id="grade_section">
    <h2>Graduation</h2>
    <form action="assessment2-2.php" method="post">
        <label for="grade">
            Grade:
            <input id="grade" type="number" name="grade" min="0" max="100" value="50">
        </label>
        <label for="level">
            Level:
            <select id="level" name="level">
                <option value="undergraduate">Undergraduate Modular Framework Awards</option>
                <option value="hnd">HND - Current Schema</option>
                <option value="masters">Masters</option>
            </select>
        </label>
        <input type="submit" value="submit">
    </form>

    <p>
        <?php
        if (isset($_POST["grade"]) and isset($_POST["level"])) {
            echo getMessage(intval($_POST["grade"]), $all_level[$_POST["level"]]);
        }
        ?>
    </p>
</section>

<section id="guess">
    <h2>Guess What is the number</h2>
    <form action="assessment2-3.php" method="post" oninput="amount.value = guess_value.value">
        <p>Number is between <?php echo $_SESSION["min"]; ?> and <?php echo $_SESSION["max"]; ?></p>
        <label for="guess_value">
            <input id="guess_value" type="range" name="guess_value"
                   min="<?php echo $_SESSION["min"] ?>" max="<?php echo $_SESSION["max"] ?>"
                   value="<?php echo $_SESSION["min"] + ($_SESSION["max"] - $_SESSION["min"]) / 2; ?>"
                   step="1">
        </label>
        <output name="amount" for="output_value">
            <?php echo ceil($_SESSION["min"] + ($_SESSION["max"] - $_SESSION["min"]) / 2); ?>
        </output>
        <input type="submit" value="submit" name="try">
        <input type="submit" value="reset" name="reset"/>
    </form>

    <?php echo $msg; ?>
</section>

<section id="form">
    <h2>Dynamic input</h2>
    <form action="assessment2-4.php" method="post">
        <label for="nbinput">
            <?php
            $nbInput = isset($_POST['nbinput']) ? $_POST['nbinput'] : 3;
            echo '<input id="nbinput" type="number" name="nbinput" min="0" value="' . $nbInput . '">';
            ?>
        </label>
        <input type="submit" name="nbform" value="update">
        <?php
        for ($i = 0; $i < $nbInput; ++$i) {
            $name = (isset($_POST['names']) and isset($_POST['names'][$i])) ? $_POST['names'][$i] : "";
            echo '<label for="name' . $i . '">';
            echo '<input id="name' . $i . '" type="text" name="names[]" placeholder="' . $name . '">';
            echo '</label>';
        }
        ?>
        <input type="submit" name="input" value="submit">
    </form>
    <ul>
        <?php
        if (isset($_POST["input"])) {
            $values = $_POST["names"];
            asort($values);
            foreach ($values as $value) {
                echo "<li>" . $value . "</li>";
            }
        }
        ?>
    </ul>
</section>

</body>
</html>