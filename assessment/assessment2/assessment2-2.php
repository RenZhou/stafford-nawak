<?php

session_start();
if (!isset($_SESSION["col"])) {
    $_SESSION["col"] = 10;
    $_SESSION["row"] = 10;
}

if (isset($_GET['col']) and isset($_GET['row'])) {
    $_SESSION['col'] = $_GET['col'];
    $_SESSION['row'] = $_GET['row'];
}

$all_level = [
    "undergraduate" => [
        "first" => [
            "grade-min" => 70,
            "grade-max" => 100,
            "message" => "70%+ Grade Points 13 - 15 First Class Honours",
        ],
        "second" => [
            "grade-min" => 60,
            "grade-max" => 69,
            "message" => "60 - 69% Grade Points 10 - 12 Upper Second Class Honours",
        ],
        "third" => [
            "grade-min" => 50,
            "grade-max" => 59,
            "message" => "50 - 59% Grade Points 7 - 9 Lower Second Class Honours",
        ],
        "fourth" => [
            "grade-min" => 40,
            "grade-max" => 49,
            "message" => "40 - 49% Grade Points 4 - 6 Third Class Honours",
        ],
        "fifth" => [
            "grade-min" => 0,
            "grade-max" => 39,
            "message" => " Fail",
        ]
    ],
    "hnd" => [
        "first" => [
            "grade-min" => 70,
            "grade-max" => 100,
            "message" => "70%+ Distinction - grade points 13 - 15 ",
        ],
        "second" => [
            "grade-min" => 53,
            "grade-max" => 69,
            "message" => "53 - 69% Merit - grade points 8 - 12 ",
        ],
        "third" => [
            "grade-min" => 40,
            "grade-max" => 52,
            "message" => "40 - 52% Pass - grade points 4 - 7",
        ],
        "fourth" => [
            "grade-min" => 0,
            "grade-max" => 39,
            "message" => "0 - 39% Fail - grade points 1 - 3",
        ]
    ],
    "masters" => [
        "first" => [
            "grade-min" => 70,
            "grade-max" => 100,
            "message" => "70%+ Distinction - grade points 13 - 15",
        ],
        "second" => [
            "grade-min" => 60,
            "grade-max" => 69,
            "message" => "60 - 69% Pass with Merit - grade points 10 - 12",
        ],
        "third" => [
            "grade-min" => 50,
            "grade-max" => 59,
            "message" => "50 - 59% Pass - grade points 7 - 9",
        ],
        "fourth" => [
            "grade-min" => 0,
            "grade-max" => 49,
            "message" => "0 - 49% Failure - grade points 1 – 6",
        ]
    ]
];

function getMessage($grade, $level)
{
    foreach ($level as $base) {
        if ($grade >= $base["grade-min"] and $grade <= $base["grade-max"]) {
            return $base["message"];
        }
    }
}

?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>PHP Assessment Exercise 2-2</title>
    <link rel="stylesheet" href="common.css">
</head>

<body>

<section id="multiplication">
    <h2>Table of multiplication</h2>
    <form action="assessment2-1.php" method="get">
        <label for="col">
            <input id="col" type="number" name="col">
        </label>
        <label for="row">
            <input id="row" type="number" name="row">
        </label>
        <input type="submit" value="submit">
    </form>
    <br>
    <table>
        <tbody>
        <?php
        for ($col = 1; $col <= $_SESSION["col"]; ++$col) {
            echo "<tr>";
            for ($row = 1; $row <= $_SESSION["row"]; ++$row) {
                echo "<td>" . $col * $row . "</td>";
            }
            echo "</tr>";
        }
        ?>
        </tbody>
    </table>
</section>

<section id="grade_section">
    <h2>Graduation</h2>
    <form action="assessment2-2.php" method="post">
        <label for="grade">
            Grade:
            <input id="grade" type="number" name="grade" min="0" max="100" value="50">
        </label>
        <label for="level">
            Level:
            <select id="level" name="level">
                <option value="undergraduate">Undergraduate Modular Framework Awards</option>
                <option value="hnd">HND - Current Schema</option>
                <option value="masters">Masters</option>
            </select>
        </label>
        <input type="submit" value="submit">
    </form>

    <p>
        <?php
        if (isset($_POST["grade"]) and isset($_POST["level"])) {
            echo getMessage(intval($_POST["grade"]), $all_level[$_POST["level"]]);
        }
        ?>
    </p>
</section>

</body>
</html>