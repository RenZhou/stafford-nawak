<?php

session_start();
if (!isset($_SESSION["col"])) {
    $_SESSION["col"] = 10;
    $_SESSION["row"] = 10;
}
if (isset($_GET['col']) and isset($_GET['row'])) {
    $_SESSION['col'] = $_GET['col'];
    $_SESSION['row'] = $_GET['row'];
}

?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">

<head>
    <meta charset="UTF-8">
    <title>PHP Assessment Exercise 2-1</title>
    <link rel="stylesheet" href="common.css">
</head>

<body>
<section id="multiplication">
    <h2>Table of multiplication</h2>
    <form action="assessment2-1.php" method="get">
        <label for="col">
            <input id="col" type="number" name="col">
        </label>
        <label for="row">
            <input id="row" type="number" name="row">
        </label>
        <input type="submit" value="submit">
    </form>
    <br>
    <table>
        <tbody>
        <?php
        for ($col = 1; $col <= $_SESSION["col"]; ++$col) {
            echo "<tr>";
            for ($row = 1; $row <= $_SESSION["row"]; ++$row) {
                echo "<td>" . $col * $row . "</td>";
            }
            echo "</tr>";
        }
        ?>
        </tbody>
    </table>
</section>

</body>
</html>