<?php

function calculAge(DateTime $birthday)
{
    $diff = $birthday->diff(new DateTime("now"));
    return $diff->format("%y");
}

?>

<!DOCTYPE html>
<html>
<head>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
    <title>My base page</title>
</head>
<body>

<form action="week3-ex3.php" method="post">
    <label>
        Your name :
        <input type="text" name="name"/>
    </label>
    <label>
        Your birthday
        <input type="date" name="birthday" value="<?php echo date("Y-m-d") ?>"/>
    </label>
    <input type="submit">
</form>

<p>Today is : <?php echo date("d/m/Y"); ?></p>

<p>
    <?php
    if (isset($_POST["name"]) and $_POST["name"] != "")
        echo $_POST["name"] . "age is : ";
    else
        echo "Your age is : ";

    if (isset($_POST["birthday"])) {
        $birthday = new DateTime($_POST["birthday"]);
        $age = calculAge($birthday);

        echo $age;
    } else
        echo "Unknow";
    ?>
</p>

</body>
</html>
