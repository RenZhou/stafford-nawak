<?php
$name = "Philip Edwards";
$email = "p.j.edwards@staffs.ac.uk";
?>

<!DOCTYPE html>
<html>
<head>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
    <title>My base page</title>
</head>
<body>

<form action="week3-ex2.php" method="post">
    <label>
        New name is :
        <input type="text" name="name"/>
    </label>
    <label>
        New Email is :
        <input type="text" name="email"/>
    </label>
    <input type="submit">
</form>

<p>Name : <?php
    if (isset($_POST["name"]) and $_POST["name"] != "")
        echo $_POST["name"];
    else
        echo $name;
    ?> </p>
<p>Email : <?php
    if (isset($_POST["email"]) and $_POST["email"] != "")
        echo $_POST["email"];
    else
        echo $email;
    ?> </p>

</body>
</html>
